# Sanctuary · Flutter

[![flutter](https://img.shields.io/static/v1?label=Flutter&message=v2.0&color=02569B&style=flat-square&logo=flutter&logoColor=ffffff)](https://flutter.dev)
[![dart](https://img.shields.io/static/v1?label=Dart&message=v2.12&color=0175C2&style=flat-square&logo=dart&logoColor=ffffff)](https://dart.dev)

[![android-min](https://img.shields.io/static/v1?label=Android%20min&message=v4.3.1&color=3DDC84&style=flat-square&logo=android&logoColor=ffffff)](https://www.android.com)

Designed to be connected to Laravel API with [**laravel/sanctum**](https://github.com/laravel/sanctum) like [**Sanctuary · Back**](https://gitlab.com/ewilan-riviere/sanctuary-back).

## Setup

Download dependencies

```bash
flutter pub get
```

Run on emulator

```bash
flutter run
```

### Production

Generate APK and move it to root repository

```bash
make apk
```

Generate APK splitted and move it to root repository

```bash
make apk-split
```

Generate App Bundle and move it to root repository

```bash
make aab
```

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

```bash
emmet stateful widget
Ctrl + . on widget
```

- <https://androidride.com/flutter-create-new-project-command-line-options/>
- <https://medium.com/flutter-community/flutter-and-the-command-line-a-love-story-a3648ef2411>
- <https://medium.com/nonstopio/flutter-best-practices-c3db1c3cd694>
- <https://medium.com/swlh/flutter-get-data-from-a-rest-api-and-save-locally-in-a-sqlite-database-9a9de5867939>
