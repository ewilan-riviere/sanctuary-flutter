import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sanctuary/providers/auth_provider.dart';
import 'package:sanctuary/screens/app.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => AuthProvider()),
        // Provider(create: (context) => SomeOtherClass()),
      ],
      child: Sanctuary(),
    ),
  );
}
