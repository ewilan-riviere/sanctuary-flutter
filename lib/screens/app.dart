import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:sanctuary/providers/auth_provider.dart';
import 'package:sanctuary/screens/home_screen.dart';
import 'package:sanctuary/screens/login_screen.dart';
import 'package:sanctuary/screens/splash_screen.dart';

bool _isAuthenticated = false;

class Sanctuary extends StatefulWidget {
  Sanctuary({Key? key}) : super(key: key);

  @override
  _SanctuaryState createState() => _SanctuaryState();
}

class _SanctuaryState extends State<Sanctuary> {
  final storage = new FlutterSecureStorage();

  Future<void> _readToken() async {
    String? token = await storage.read(key: 'token');
    if (token != null) {
      await Provider.of<AuthProvider>(context, listen: false)
          .tryToken(token: token);
    }
    _isAuthenticated =
        Provider.of<AuthProvider>(context, listen: false).authenticated;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Replace the 3 second delay with your initialization code:
      future: _readToken(),
      builder: (context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return MaterialApp(home: SplashScreen());
        } else {
          if (_isAuthenticated) {
            return HomeScreen();
          } else {
            return LoginScreen();
          }
        }
      },
    );
  }
}
