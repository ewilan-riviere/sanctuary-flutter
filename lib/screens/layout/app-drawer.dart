import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sanctuary/providers/auth_provider.dart';
import 'package:sanctuary/screens/login_screen.dart';
import 'package:sanctuary/utils/theme.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Consumer<AuthProvider>(
        builder: (context, auth, child) {
          return ListView(
            children: [
              DrawerHeader(
                child: Column(
                  children: [
                    CircleAvatar(
                      backgroundImage: NetworkImage(auth.user.avatar),
                      radius: 30,
                    ),
                    SizedBox(height: 10),
                    Text(
                      auth.user.name,
                      style: TextStyle(color: Colors.white),
                    ),
                    SizedBox(height: 10),
                    Text(
                      auth.user.email,
                      style: TextStyle(color: Colors.white),
                    )
                  ],
                ),
                decoration: BoxDecoration(
                  color: AppTheme.lightTheme.accentColor,
                ),
              ),
              ListTile(
                title: Text('Logout'),
                leading: Icon(Icons.logout),
                onTap: () {
                  Provider.of<AuthProvider>(context, listen: false).logout();
                  // Navigator.of(context).push(
                  //   MaterialPageRoute(
                  //     builder: (context) => LoginScreen(),
                  //   ),
                  // );
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LoginScreen()),
                  );
                },
              )
            ],
          );
        },
      ),
    );
  }
}
