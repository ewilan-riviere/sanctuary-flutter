import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:sanctuary/providers/auth_provider.dart';
import 'package:sanctuary/screens/layout/app-drawer.dart';
import 'package:sanctuary/utils/theme.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final storage = new FlutterSecureStorage();

  @override
  void initState() {
    super.initState();
  }

  void readToken() async {
    String? token = await storage.read(key: 'token');
    if (token != null) {
      Provider.of<AuthProvider>(context, listen: false).tryToken(token: token);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: AppTheme.lightTheme,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Sanctuary'),
        ),
        body: Center(
          child: Text('Home screen'),
        ),
        drawer: AppDrawer(),
        // floatingActionButton: Builder(
        //   builder: (context) => // Ensure Scaffold is in context
        //       IconButton(
        //     icon: Icon(Icons.menu),
        //     onPressed: () => Scaffold.of(context).openDrawer(),
        //   ),
        // ),
      ),
    );
  }
}
