import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image(
              image: AssetImage('assets/images/icon.png'),
              // width: 300,
              height: 150,
              fit: BoxFit.fill,
            ),
            Padding(
              padding: EdgeInsets.only(top: 5),
              child: Text(
                'Sanctuary',
                textAlign: TextAlign.center,
                // style: TextStyle(
                //   fontSize: 42.0,
                //   fontWeight: FontWeight.w700,
                // ),
                style: GoogleFonts.sourceCodePro(
                  fontSize: 48,
                  fontWeight: FontWeight.w700,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
