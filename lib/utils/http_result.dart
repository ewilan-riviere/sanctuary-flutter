import 'dart:convert';

HttpResult httpResultFromJson(String str) =>
    HttpResult.fromJson(jsonDecode(str));

String httpResultToJson(HttpResult data) => jsonEncode(data.toJson());

class HttpResult {
  final bool? success;
  final String? message;

  HttpResult({required this.success, required this.message});

  HttpResult.fromJson(Map<String, dynamic> json)
      : success = json['success'],
        message = json['message'];

  Map<String, dynamic> toJson() => {
        "success": success,
        "message": message,
      };
}
