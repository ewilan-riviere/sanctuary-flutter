import 'package:logger/logger.dart';

class Log {
  /// Return simple logger
  Logger get logger {
    return Logger(
      printer: PrettyPrinter(methodCount: 0, lineLength: 50),
    );
  }

  static warn(dynamic message) {
    message = message.toString();
    Log().logger.w(message);
  }

  static info(String message) {
    Log().logger.i(message);
  }

  static danger(String message) {
    Log().logger.d(message);
  }
}
