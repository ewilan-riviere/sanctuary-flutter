// import 'package:json_annotation/json_annotation.dart';

// @JsonSerializable()
// class User {
//   final String name;
//   final String email;
//   final String avatar;

//   User({required this.name, required this.email, required this.avatar});
// }

import 'dart:convert';

User userFromJson(String str) => User.fromJson(jsonDecode(str));

String userToJson(User data) => jsonEncode(data.toJson());

class User {
  final String name;
  final String email;
  final String avatar;

  User({required this.name, required this.email, required this.avatar});

  User.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        email = json['email'],
        avatar = json['avatar'];

  // factory User.fromJson(Map<String, dynamic> json) => User(
  //       name: json["name"],
  //       email: json["email"],
  //       avatar: json["avatar"],
  //     );

  Map<String, dynamic> toJson() => {
        "name": name,
        "email": email,
        "avatar": avatar,
      };
}
