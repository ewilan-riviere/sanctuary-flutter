import 'package:flutter/material.dart';

// ignore: must_be_immutable
class AppSwitch extends StatefulWidget {
  bool currentValue;
  final String label;

  AppSwitch({Key? key, required this.currentValue, required this.label})
      : super(key: key);

  @override
  _AppSwitchState createState() => _AppSwitchState();
}

class _AppSwitchState extends State<AppSwitch> {
  late bool currentValue;
  void _switched(value) {
    setState(() {
      widget.currentValue = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 60,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: 200,
            child: Text(widget.label),
          ),
          Switch(
            value: widget.currentValue,
            onChanged: _switched,
            activeTrackColor: Colors.red.shade200,
            activeColor: Theme.of(context).primaryColor,
          )
        ],
      ),
      // child: Card(
      //   elevation: 3,
      //   child: Padding(
      //     padding: EdgeInsets.symmetric(horizontal: 10),
      //     child: Row(
      //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //       children: [
      //         Padding(
      //           padding: EdgeInsets.all(5),
      //           child: Text(widget.label),
      //         ),
      //         Switch(
      //           value: widget.currentValue,
      //           onChanged: _switched,
      //           activeTrackColor: Colors.red.shade200,
      //           activeColor: Theme.of(context).primaryColor,
      //         ),
      //       ],
      //     ),
      //   ),
      // ),
    );
  }
}
