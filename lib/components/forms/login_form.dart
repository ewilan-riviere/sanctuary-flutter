import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sanctuary/components/input/app_input_text.dart';
import 'package:sanctuary/components/input/text_formatter.dart';
import 'package:sanctuary/providers/auth_provider.dart';
import 'package:sanctuary/screens/home_screen.dart';
import 'package:sanctuary/utils/http_result.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  final FocusScopeNode _node = FocusScopeNode();
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  late String _deviceName;
  bool _isLoading = false;

  void getDeviceInfo() async {
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      _deviceName = androidInfo.model;
    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      _deviceName = iosInfo.model;
    } else {
      _deviceName = 'unknown';
    }
  }

  @override
  void initState() {
    super.initState();
    _emailController.text = 'admin@mail.com';
    _passwordController.text = 'password';
    this.getDeviceInfo();
  }

  @override
  void dispose() {
    _node.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: FocusScope(
        node: _node,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AppInputText(
              controller: _emailController,
              formatters: [
                LowerCaseTextFormatter(),
              ],
              textInputType: TextInputType.emailAddress,
              onEditingCompleteMethod: _node.nextFocus,
              textInputAction: TextInputAction.next,
              autofocus: false,
            ),
            AppInputText(
              controller: _passwordController,
              textInputAction: TextInputAction.done,
              obscureText: true,
              onEditingCompleteMethod: () {
                _node.unfocus();
                _login();
              },
            ),
            // TextFormField(
            //   controller: _emailController,
            //   validator: (value) => value!.isEmpty ? 'Set a email' : null,
            // ),
            // TextFormField(
            //   controller: _passwordController,
            //   validator: (value) =>
            //       value!.isEmpty ? 'Set a password' : null,
            // ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 5, horizontal: 3),
              child: Stack(
                children: [
                  SizedBox(
                    width: double.infinity,
                    // height: double.infinity,
                    child: ElevatedButton(
                      onPressed: () => _isLoading ? null : _login(),
                      child: _isLoading
                          ? SizedBox(
                              height: 15,
                              width: 15,
                              child: CircularProgressIndicator(
                                strokeWidth: 3,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                  Colors.red.shade100,
                                ),
                              ),
                            )
                          : Text('Sign in'),
                      style: ElevatedButton.styleFrom(
                        minimumSize: Size(300.0, 40.0),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _login() async {
    setState(() {
      this._isLoading = true;
    });
    Map credentials = {
      'email': _emailController.text,
      'password': _passwordController.text,
      'device_name': _deviceName,
    };
    _formKey.currentState!.context;

    if (_formKey.currentState!.validate()) {
      HttpResult result =
          await Provider.of<AuthProvider>(context, listen: false)
              .login(credentials: credentials);
      if (result.success!) {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => HomeScreen(),
          ),
        );
      } else {
        Future.delayed(Duration(milliseconds: 500), () {
          ScaffoldMessenger.of(context).showSnackBar(
            getSnackBar(message: result.message!),
          );
        });
        setState(() {
          this._isLoading = false;
        });
      }
    }
  }

  SnackBar getSnackBar({required String message}) {
    return SnackBar(
      backgroundColor: Colors.redAccent,
      action: SnackBarAction(
        label: 'Dismiss',
        textColor: Colors.grey.shade300,
        onPressed: () {
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
        },
      ),
      content: Text(
        message,
        style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w600),
      ),
    );
  }

  Widget getToast({required String message}) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Colors.redAccent,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            Icons.report_gmailerrorred_outlined,
            color: Colors.white,
          ),
          SizedBox(
            width: 12.0,
          ),
          Text(
            message,
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
    );
  }
}
