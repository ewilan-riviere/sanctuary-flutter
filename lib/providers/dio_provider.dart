import 'package:dio/dio.dart';

Dio dio() {
  Dio dio = new Dio();

  // iOS
  // dio.options.baseUrl = 'http://localhost:8000/api';
  // Android
  dio.options.baseUrl = 'http://10.0.2.2:8000/api';

  dio.options.headers['accept'] = 'Application/Json';

  return dio;
}

// class ApiBaseHelper {
//   static final String url = 'BASE_URL';
//   static BaseOptions opts = BaseOptions(
//     baseUrl: url,
//     responseType: ResponseType.json,
//     connectTimeout: 30000,
//     receiveTimeout: 30000,
//   );

//   static Dio createDio() {
//     return Dio(opts);
//   }

//   static Dio addInterceptors(Dio dio) {
//     return dio
//       ..interceptors.add(
//         InterceptorsWrapper(
//             onRequest: (RequestOptions options) => requestInterceptor(options),
//             onError: (DioError e) async {
//               return e.response.data;
//             }),
//       );
//   }

//   static dynamic requestInterceptor(RequestOptions options) async {
//     // Get your JWT token
//     const token = '';
//     options.headers.addAll({"Authorization": "Bearer: $token"});
//     return options;
//   }

//   static final dio = createDio();
//   static final baseAPI = addInterceptors(dio);

//   Future<Response> getHTTP(String url) async {
//     try {
//       Response response = await baseAPI.get(url);
//       return response;
//     } on DioError catch (e) {
//       // Handle error
//     }
//   }

//   Future<Response> postHTTP(String url, dynamic data) async {
//     try {
//       Response response = await baseAPI.post(url, data: data);
//       return response;
//     } on DioError catch (e) {
//       // Handle error
//     }
//   }

//   Future<Response> putHTTP(String url, dynamic data) async {
//     try {
//       Response response = await baseAPI.put(url, data: data);
//       return response;
//     } on DioError catch (e) {
//       // Handle error
//     }
//   }

//   Future<Response> deleteHTTP(String url) async {
//     try {
//       Response response = await baseAPI.delete(url);
//       return response;
//     } on DioError catch (e) {
//       // Handle error
//     }
//   }
// }
