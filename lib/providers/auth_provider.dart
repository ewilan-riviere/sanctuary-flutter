import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:sanctuary/models/user.dart';
import 'package:sanctuary/providers/dio_api_provider.dart';
import 'package:sanctuary/providers/dio_provider.dart';
import 'package:sanctuary/utils/http_result.dart';
import 'package:sanctuary/utils/log.dart';

class AuthProvider extends ChangeNotifier {
  bool _isLoggedIn = false;
  late User _user;
  late String _token;
  final storage = new FlutterSecureStorage();

  bool get authenticated => _isLoggedIn;
  User get user => _user;
  String get token => _token;

  Future<HttpResult> login({required Map credentials}) async {
    bool _success = false;
    String _message = 'Server error';

    try {
      DioBase api = DioBase();
      Response? response =
          await api.postHTTP(endpoint: '/sanctum/token', data: credentials);
      print(response?.realUri);
      if (response?.statusCode == 200) {
        String token = (response?.data).toString();
        _success = true;
        this.tryToken(token: token);
        notifyListeners();
      }
    } on DioError catch (e) {
      if (e.response != null) {
        int? statusCode = e.response?.statusCode;
        Log.warn('Error: $statusCode on ${e.response?.realUri}');
        Map<String, dynamic> json = e.response?.data;
        _message = json['message'];
      } else {
        Log.warn('Response is null');
        _message = 'Servor error';
        print(e.message);
      }
    }

    HttpResult httpResult =
        new HttpResult(success: _success, message: _message);

    return httpResult;
  }

  Future<bool> tryToken({required String token}) async {
    bool success = false;
    Log.warn(token);
    try {
      Response response;
      BaseOptions options = BaseOptions(
        baseUrl: DioBase.url,
        headers: {
          "Accept": 'application/json',
          "Authorization": "Bearer: $token"
        },
      );
      Dio dio = Dio(options);
      response = await dio.get('/user');

      // DioBase api = DioBase();
      // Response? response = await api.getHTTP('/user');

      if (response.statusCode == 200) {
        this._isLoggedIn = true;
        this._user = User.fromJson(response.data);
        this._token = token;
        this.storeToken(token: token);
        notifyListeners();
        success = true;
      } else {
        // Log.warn(response.statusCode);
        // Log.warn(response.data);
      }
    } on DioError catch (e) {
      Log.warn(e.response?.statusCode.toString());
      Log.warn(e.response?.data.toString());
    }
    // options: Dio.Options(
    //     headers: {
    //       'Authorization': 'Bearer $token',
    //     },
    //   ),

    // try {

    //   }
    // } catch (e) {
    //   Log.warn('Error on /user endpoint');
    //   Log.warn(e.toString());
    // }

    return success;
  }

  void storeToken({required String token}) async {
    await this.storage.write(key: 'token', value: token);
  }

  void logout() async {
    try {
      await dio().get(
        '/sanctum/revoke',
        // options: BaseOptions(
        //   headers: {
        //     'Authorization': 'Bearer $token',
        //   },
        // ),
      );
      this.clean();
      notifyListeners();
    } catch (e) {}
  }

  void clean() async {
    this._isLoggedIn = false;
    this._token = '';
    await this.storage.delete(key: 'token');
  }
}
