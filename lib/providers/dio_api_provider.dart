import 'package:dio/dio.dart';
import 'package:sanctuary/providers/environment.dart';
import 'package:sanctuary/utils/log.dart';

class DioBase {
  static final String url = EnvironmentConfig.APP_URL;
  static BaseOptions opts = BaseOptions(
    baseUrl: url,
    responseType: ResponseType.json,
    connectTimeout: 30000,
    receiveTimeout: 30000,
    headers: {Headers.acceptHeader: 'application/json'},
  );

  static Dio createDio() {
    return Dio(opts);
  }

  // static Dio addInterceptors(Dio dio) {
  //   return dio
  //     ..interceptors.add(
  //       InterceptorsWrapper(
  //         onRequest: (RequestOptions options, handler) =>
  //             requestInterceptor(options),
  //         onError: (e, handler) {
  //           return e.response?.data;
  //         },
  //       ),
  //     );
  // }

  static Dio addInterceptors(Dio dio) {
    return dio
      ..interceptors.add(
        InterceptorsWrapper(
          onRequest: (options, handler) => requestInterceptor(options),
          onResponse: (e, handler) {
            return e.data;
          },
        ),
      );
  }

  static dynamic requestInterceptor(RequestOptions options) async {
    // Get your JWT token
    const token = '';
    options.headers.addAll({"Authorization": "Bearer: $token"});
    return options;
  }

  static final Dio dio = createDio();
  static final Dio baseAPI = addInterceptors(dio);

  Future<Response?> getHTTP(String url) async {
    try {
      Response response = await baseAPI.get(url);
      return response;
    } on DioError catch (e) {
      Log.warn((e.response?.statusCode).toString());
      return e.response;
    }
  }

  /// Execute `Dio` request with `POST` method
  Future<Response?> postHTTP(
      {required String endpoint, required dynamic data}) async {
    print(data.toString());
    // print('baseAPI $baseAPI');
    try {
      Response response = await dio.post(endpoint, data: data);
      print(response.statusCode);
      return response;
    } on DioError catch (e) {
      return e.response;
    }

    // try {
    //   Response response = await baseAPI.post(endpoint, data: data);
    //   print(response.data.toString());
    //   return response;
    // } on DioError catch (e) {
    //   print('response');
    //   Log.warn('${this.runtimeType} ' + (e.response?.statusCode).toString());
    //   return e.response;
    // }
  }

  Future<Response?> putHTTP(String url, dynamic data) async {
    try {
      Response response = await dio.put(url, data: data);
      return response;
    } on DioError catch (e) {
      // Handle error
      return e.response;
    }
  }

  Future<Response?> deleteHTTP(String url) async {
    try {
      Response response = await dio.delete(url);
      return response;
    } on DioError catch (e) {
      // Handle error
      return e.response;
    }
  }
}
