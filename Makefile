# .SILENT: generate all
.ONESHELL:

# flutter run --dart-define=DEFINE_APP_URL=https://sanctuary.git-projects.xyz/api

clean:
	flutter clean
	flutter pub get
	rm -f *.apk
	rm -f *.aab
	
apk: clean
	flutter build apk --release
	cp build/app/outputs/flutter-apk/app-release.apk ./

apk-split: clean
	flutter build apk --split-per-abi
	cp build/app/outputs/flutter-apk/app-armeabi-v7a-release.apk ./

aab: clean
	flutter build appbundle --release
	cp build/app/outputs/bundle/release/app-release.aab ./

runner:
	flutter pub run build_runner watch --delete-conflicting-outputs


